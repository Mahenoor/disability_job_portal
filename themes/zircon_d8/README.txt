Theme Information
-------------------------------------------------------------------------------
Zircon Drupal 8
- Theme Version: 8.1.1
- Released on: May 1st, 2014
- Packaged on: Jul 06, 2016
- Compatible with Drupal 8.x
- Copyright (C) 2012-2014 WeebPal.com. All Rights Reserved.
- @license - GNU/GPL, http://www.gnu.org/licenses/gpl.html
- Author: WeebPal Team
- Website: http://www.weebpal.com
- Guide: http://weebpal.com/guides
- FAQ: http://weebpal.com/faq
- Twitter: http://twitter.com/weebpal
- Facebook: http://www.facebook.com/weebpal


Folder Structure
-------------------------------------------------------------------------------
- README.txt
- zircon_d8-demo-8.1.1.zip demo for Zircon Drupal 8
- zircon_d8-8.1.1.zip: Zircon Drupal 8 theme


Installation
-------------------------------------------------------------------------------
From Demo package:
http://weebpal.com/guide/step-step-guide-install-drupal-8-theme

