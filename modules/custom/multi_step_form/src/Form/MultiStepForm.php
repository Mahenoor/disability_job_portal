<?php

/**
 * @file
 * Contains Drupal\multi_step\Form\MultiStepForm.
 */

namespace Drupal\multi_step\Form;

use \Drupal\Core\Routing\RouteMatch;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use \Drupal\node\Entity\Node;
use \Drupal\file\Entity\File;
use Drupal\taxonomy\Entity\Term;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\taxonomy\TermStorage;
use Drupal\node\NodeInterface;
use Drupal\Core\Entity\EntityInterface;


class MultiStepForm extends ConfigFormBase
{
    protected $step = 1;
  /**
   * {@inheritdoc}
   */
    protected function getEditableConfigNames() {}

  /**
   * {@inheritdoc}
   */
    public function getFormID() {
        return 'multi_step_form';
    }

  /**
   * {@inheritdoc}
   */
    public function buildForm(array $form, FormStateInterface $form_state) {
        $tree = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('category');
        $category = array();
        $categoryArray = array();
        for ($i = 0; $i < sizeof($tree); $i++) {
            $categoryTermName=taxonomy_term_load($tree[$i]->tid)->get('name')->value;
            array_push($categoryArray, $categoryTermName);
          }
        $tree = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('educational_details');
        $educationalDetails = array();
        $educationalDetailsArray = array();
        for ($i=0;$i<sizeof($tree);$i++) {
            $educationalDetailsTermName = taxonomy_term_load($tree[$i]->tid)->get('name')->value;
            array_push($educationalDetailsArray, $educationalDetailsTermName);
        }
        $tree = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('disability_type');
        $disabilityType = array();
        $disabilityTypeArray = array();
        for ($i=0;$i<sizeof($tree);$i++) {
            $disabilityTypeTermName = taxonomy_term_load($tree[$i]->tid)->get('name')->value;
            array_push($disabilityTypeArray,$disabilityTypeTermName);
        }
        $tree = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('disability_area');
        $disabilityArea=array();
        $disabilityAreaArray = array();
        for ($i=0;$i<sizeof($tree);$i++) {
            $disabilityAreaTermName=taxonomy_term_load($tree[$i]->tid)->get('name')->value;
            array_push($disabilityAreaArray,$disabilityAreaTermName);
        }
        $tree = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('occupation');
        $occupation=array();
        $occupationArray = array();
        for ($i=0;$i<sizeof($tree);$i++) {
            $occupationTermName=taxonomy_term_load($tree[$i]->tid)->get('name')->value;
            array_push($occupationArray,$occupationTermName);
        }
        $tree = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('blood_group');
         
        $bloodGroup=array();
        $bloodGroupArray = array();
        for($i=0;$i<sizeof($tree);$i++){
            $bloodGroupTermName=taxonomy_term_load($tree[$i]->tid)->get('name')->value;
            array_push($bloodGroupArray,$bloodGroupTermName);
        }
        $tree = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('address_proof');

        $addressProof=array();
        $addressProofArray = array();
        for($i=0;$i<sizeof($tree);$i++){
            $addressProofTermName=taxonomy_term_load($tree[$i]->tid)->get('name')->value;
            array_push($addressProofArray,$addressProofTermName);
        }
        $tree = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('personal_income');
        $personalIncome=array();
        $personalIncomeArray = array();
        for($i=0;$i<sizeof($tree);$i++){
            $personalIncomeTermName=taxonomy_term_load($tree[$i]->tid)->get('name')->value;
            array_push($personalIncomeArray,$personalIncomeTermName);
        }
        $tree = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('father_income');
        $fatherIncome = array();
        $fatherIncomeArray = array();
        for($i=0;$i<sizeof($tree);$i++) {
            $fatherIncomeTermName = taxonomy_term_load($tree[$i]->tid)->get('name')->value;
            array_push($fatherIncomeArray,$fatherIncomeTermName);
        }
        $tree = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('spouse_income');
        $spouseIncome = array();
        $spouseIncomeArray = array();
        for($i=0;$i<sizeof($tree);$i++){
            $spouseIncomeTermName = taxonomy_term_load($tree[$i]->tid)->get('name')->value;
            array_push($spouseIncomeArray,$spouseIncomeTermName);
        }
       
        $form = parent::buildForm($form, $form_state);
        $config = $this->config('multi_step.multi_step_form_config');
      //  if ($this->step == 1) {
            $fieldCasteCertificate = $form_state->getValue('field_caste_certificate');
           // var_dump($fieldCasteCertificate);
            if ($fieldCasteCertificate) {
                $fid = $fieldCasteCertificate[0]; 
                $file_name = File::load($fid)->getFilename();
                echo $file_name;
            }
            $fieldPhotograph = $form_state->getValue('field_photograph');
           // var_dump($fieldPhotograph);
            if ($fieldPhotograph) {
                $fid = $fieldPhotograph[0]; 
                var_dump($fid);exit;
                $file_name = File::load($fid)->getFilename();
                echo $file_name;
            }
            $fieldDisabilityCertificate = $form_state->
            getValue('field_disability_certificate');
            //var_dump($fieldDisabilityCertificate);
            if ($fieldDisabilityCertificate) {
                $fid =  $fieldDisabilityCertificate[0]; 
                $file_name = File::load($fid)->getFilename();
                echo $file_name;
            }
            $form['field_applicant_name']['#prefix'] = '<fieldset>';
            $form['field_applicant_name'] = [
                '#type' => 'textfield',
                '#title' => $this->t('(1)Applicant Name :<span style="color:red">*</span>'),
                '#description' => $this->t(''),
                '#required' => false,
                '#default_value' => $config->get('field_applicant_name'),
             //'#default_value' => ($config->get('field_applicant_name')?$config->get('field_applicant_name'):''),
            ];
            $form['field_father_s_name'] = [
                '#type' => 'textfield',
                '#title' => $this->t('(2)Father\'s Name :<span style="color:red">*</span>'),
                '#description' => $this->t(''),
                '#required' => false,
                '#default_value' => $config->get('field_father_s_name'),
            ];
             $form['field_mother_s_name'] = [
                '#type' => 'textfield',
                '#title' => $this->t('(3) Mother\'s Name :<span style="color:red">*</span>'),
                '#description' => $this->t(''),
                '#required' => false,
                '#default_value' => $config->get('field_mother_s_name'),
            ];
            $form['field_date_of_birth'] = [
                '#type' => 'date',
                '#title' => $this->t('(4) Date Of Birth :<span style="color:red">*</span>'),
                '#description' => $this->t(''),
                '#required' => false,
                '#default_value' => $config->get('field_date_of_birth'),
            ];
            $form['field_age'] = [
                '#type' => 'number',
                '#title' => $this->t('(5) Age :<span style="color:red">*</span>'),
                '#description' => $this->t(''),
                 '#required' => false,
                '#default_value' => $config->get('field_age'),
            ];
            $form['field_mobile_number'] = [
                '#type' => 'tel',
                '#title' => $this->t('(6) Mobile Number :<span style="color:red">*</span>'),
                '#description' => $this->t(''),
                 '#required' => false,
                '#default_value' => $config->get('field_mobile_number'),
            ];
            $form['field_e_mail_id'] = [
                '#type' => 'email',
                '#title' => $this->t('(7) Email :<span style="color:red">*</span>'),
                '#description' => $this->t(''),
                '#required' => false,
                '#default_value' => $config->get('field_e_mail_id '),
            ];
            $form['field_gender'] = [
                '#type' => 'radios',
                '#title' => '(8) Gender :<span style="color:red">*</span>',
                '#required' => false,
                '#options' => array(male => 'Male', female => 'Female',other=>'Other'),
                '#default_value' => $config->get('field_gender'),
            ];
            $form['field_mark_of_identification'] = [
                '#type' => 'textfield',
                '#title' => $this->t('(9) Mark OF Identification :<span style="color:red">*</span>'),
                '#description' => $this->t(''),
                '#required' => false,
                '#default_value' => $config->get('field_mark_of_identification'),
            ];
            $form['field_category'] = [
                '#type' => 'radios',
                '#title' => $this->t('(10) Category :<span style="color:red">*</span>'),
                '#description' => $this->t(''),
                '#required' => false,
                '#options' => $categoryArray,
                '#default_value' => $config->get('field_category'),
            ];
            $form['field_caste_certificate'] = [
                '#type' => 'managed_file',
                '#title' => $this->t('*Caste Certificate for SC,ST,OBC'),
                '#description' => $this->t(''),
                '#required' => false,
               '#default_value' => $file_name,
               //'#default_value' => $config->get('field_caste_certificate'),

            ];
            $form['field_blood_group'] = [
                '#type' => 'radios',
                '#title' => $this->t('(11) Blood Group :<span style="color:red">*</span>'),
                 '#required' => false,
                '#description' => $this->t(''),
                '#options' => $bloodGroupArray,
                '#default_value' => $config->get('field_blood_group'),
            ];
            $form['field_marital_status'] = [
                '#type' => 'radios',
                '#title' => $this->t('(12) Marital Status :<span style="color:red">*</span>'),
                '#required' => false,
                '#description' => $this->t(''),
                '#options' => array(married=>'Married',Unmarried=>'Unmarried',Widow=>'Widow',
                  Divorced=>'Divorced',DivorceeAndWidower=>'Divorcee and widower'),
                '#default_value' => $config->get('field_marital_status'),
            ];
             $form['field__if_you_are_married_give_s'] = [
                '#type' => 'textfield',
                '#title' => $this->t('*If you are married give Spouse name :'),
                '#description' => $this->t(''),
                '#required' => false,
                '#default_value' => $config->get('field__if_you_are_married_give_s'),
            ];
             $form['field_name_of_guardian_caretaker'] = [
                '#type' => 'textfield',
                '#title' => $this->t('(13) Guardian/Caretaker Name :<span style="color:red">*</span>'),
                '#description' => $this->t(''),
                '#required' => false,
                '#default_value' => $config->get('field_name_of_guardian_caretaker'),
            ];
            $form['field_guardian_caretaker_contact'] = [
                '#type' => 'tel',
                '#title' => $this->t('(14) Caretaker Contact Number :<span style="color:red">*</span>'),
                '#description' => $this->t(''),
                '#required' => false,
                '#default_value' => $config->get('field_guardian_caretaker_contact'),
            ];
             $form['field_relation_with_person_with_'] = [
                '#type' => 'radios',
                '#title' => $this->t('(15) Relation with Person with disability :<span style="color:red">*</span>'),
                '#description' => $this->t(''),
                '#required' => false,
                '#options' => array(father=>'Father',mother=>'Mother',wife=>'Wife',husband=>'Husband',uncle=>'Uncle',aunty=>'Aunty',sister=>'Sister',other=>'Other'),
                '#default_value' => $config->get('field_relation_with_person_with_'),
            ];
            $form['field_educational_details'] = [
                '#type' => 'radios',
                '#title' => $this->t('(16) Education Details :<span style="color:red">*</span>'),
                '#description' => $this->t(''),
                '#required' => false,
                '#options' => $educationalDetailsArray,
                '#default_value' => $config->get('field_educational_details'),
            ];
            $form['field_photograph'] = [
                '#type' => 'managed_file',
                '#title' => $this->t('(17) Photograph :<span style="color:red">*</span>'),
                '#description' => $this->t(''),
                '#required' => false,
                '#default_value' => $file_name,
            ];
            $form['field_photograph']['#suffix'] = '</fieldset>';
            
       // }
       // if ($this->step == 2) {
            $form['field_correspondence_address_']['#prefix'] = '<fieldset>';
            $form['field_correspondence_address_'] = [
                '#type' => 'textfield',
                '#title' => $this->t('(1)Correspondence Address :<span style="color:red">*</span>'),
                '#description' => $this->t(''),
                '#default_value' => $config->get('field_correspondence_address_'),
            ];
            $form['field_pincode_of_correspondence_'] = [
                '#type' => 'number',
                '#title' => $this->t('(2)Pincode :<span style="color:red">*</span>'),
                '#description' => $this->t(''),
                '#default_value' => $config->get('field_pincode_of_correspondence_'),
            ];
            $form['field_state_ut_s_of_corresponden'] = [
                '#type' => 'textfield',
                '#title' => $this->t('(3)State/UT\'s :<span style="color:red">*</span'),
                '#description' => $this->t(''),
                '#default_value' => $config->get('field_state_ut_s_of_corresponden'),
            ];
            $form['field_district_of_correspondence'] = [
                '#type' => 'textfield',
                '#title' => $this->t('(4) District :<span style="color:red">*</span'),
                '#description' => $this->t(''),
                '#default_value' => $config->get('field_district_of_correspondence'),
            ];
            $form['field_city_district_of_correspon'] = [
                '#type' => 'textfield',
                '#title' => $this->t('(5) Sub District/Tehsil :<span style="color:red">*</span>'),
                '#description' => $this->t(''),
                '#default_value' => $config->get('field_city_district_of_correspon'),
            ];
            $form['field_corres_is_equal_to_permane'] = [
                '#type' => 'checkboxes',
                '#title' => $this->t('(6) Village/Block :<span style="color:red">*</span>'),
                '#description' => $this->t(''),
                '#default_value' => $config->get('field_corres_is_equal_to_permane'),
                '#attributes' => array('onclick'=>'FillAddress(this.form)'),

            ];
            $form['field_village_block_of_correspon'] = [
                '#type' => 'textfield',
                '#title' => $this->t('(6) Village/Block :<span style="color:red">*</span>'),
                '#description' => $this->t(''),
                '#default_value' => $config->get('field_village_block_of_correspon'),
            ];
            $form['field_document_for_address_proof'] = [
                '#type' => 'checkboxes',
                '#title' => $this->t('(7) Documents for address proof :<span style="color:red">*</span>'),
                '#description' => $this->t(''),
                //'#options' => $addressProofArray,
                '#options' => array(drivingLicense=>'Driving License',rationCard=>'Ration Card',voterID=>'Voter ID',other=>'Other(domicile certificate)'),
                '#default_value' => $config->get('field_document_for_address_proof'),
            ];
            $form['field_applicant_permanent_addres'] = [
                '#type' => 'textfield',
                '#title' => $this->t('(8)Permanent Address :<span style="color:red">*</span>'),
                '#description' => $this->t(''),
                '#default_value' => $config->get('field_applicant_permanent_addres'),
            ];
            $form['field_pincode_of_permanent_addre'] = [
                '#type' => 'number',
                '#title' => $this->t('(9) Pincode :<span style="color:red">*</span>'),
                '#description' => $this->t(''),
                '#default_value' => $config->get('field_pincode_of_permanent_addre'),
            ];
            $form['field_state_ut_s_of_permanent_ad'] = [
                '#type' => 'textfield',
                '#title' => $this->t('(10) State/UT\'s :<span style="color:red">*</span>'),
                '#description' => $this->t(''),
                '#default_value' => $config->get('field_state_ut_s_of_permanent_ad'),
            ];
            $form['field_district_of_permanent_addr'] = [
                '#type' => 'textfield',
                '#title' => $this->t('(11) District :<span style="color:red">*</span>'),
                '#description' => $this->t(''),
                '#default_value' => $config->get('field_district_of_permanent_addr'),
            ];
            $form['field_city_district_of_permanent'] = [
                '#type' => 'textfield',
                '#title' => $this->t('(12) City/Sub-district/Tehsil :<span style="color:red">*</span>'),
                '#description' => $this->t(''),
                '#default_value' => $config->get('field_city_district_of_permanent'),
            ];
            $form['field_village_of_permanent_addre'] = [
                '#type' => 'textfield',
                '#title' => $this->t('(13) Village/Block :<span style="color:red">*</span>'),
                '#description' => $this->t(''),
                '#default_value' => $config->get('field_village_of_permanent_addre'),
            ];
            $form['field_village_of_permanent_addre']['#suffix'] = '</fieldset>';
        // }
        // if ($this->step == 3) {
            $form['field_have_disability_certificat']['#prefix'] = '<fieldset>';
            $form['field_have_disability_certificat'] = [
                '#type' => 'radios',
                '#title' => $this->t('(1) Have Disability Certificate :<span style="color:red">*</span>'),
                '#description' => $this->t(''),
                '#required' => false,
                '#options' => array(yes=>'Yes*',no=>'No'),
                '#default_value' => $config->get('field_have_disability_certificat'),
            ];
            $form['field_disability_certificate'] = [
                '#type' => 'managed_file',
                '#title' => $this->t('*If Yes,please fill in the follwoing details and attach disability certificate:'),
                '#description' => $this->t(''),
                '#required' => false,
                '#default_value' => $file_name,
            ];
            $form['field_sr_reg_no_of_certificate'] = [
                '#type' => 'textfield',
                '#title' => $this->t('(2) Sr./Reg. No. Of Certificat e:<span style="color:red">*</span>'),
                '#description' => $this->t(''),
                '#required' => false,
                '#default_value' => $config->get('field_sr_reg_no_of_certificate'),
            ];
            $form['field_date_of_issue_'] = [
                '#type' => 'date',
                '#title' => $this->t('(3) Date Of Issue :<span style="color:red">*</span>'),
                '#description' => $this->t(''),
                '#required' => false,
                '#default_value' => $config->get('field_date_of_issue_'),
            ];
            $form['field_disability_percentage_'] = [
                '#type' => 'number',
                '#title' => $this->t('(4) Disability Percentage(%)(For example : 30%,40%,50%,60%)<span style="color:red">*</span>'),
                '#description' => $this->t(''),
                '#required' => false,
                '#default_value' => $config->get('field_disability_percentage_'),
            ];
            $form['field_details_of_issuing_authori'] = [
                '#type' => 'radios',
                '#title' => $this->t('(5) Details Of Issuing Authority  :<span style="color:red">*</span>'),
                '#description' => $this->t(''),
                '#required' => false,
                '#options' => array(chiefMedicalOffice=>'Chief Medical Office',
                        medicalAuthority=>'Medical Authority'),
                '#default_value' => $config->get('field_details_of_issuing_authori'),
            ];
            $form['field_disability_type'] = [
                '#type' => 'checkboxes',
                '#title' => $this->t('(6) Disability Type :<span style="color:red">*</span>'),
                '#description' => $this->t(''),
                '#required' => false,
                '#options' => $disabilityTypeArray,
                '#default_value' => $config->get('field_disability_type'),
            ];
            $form['field_disability_by_birth'] = [
                '#type' => 'radios',
                '#title' => $this->t('(7) Disability By Birth :<span style="color:red">*</span>'),
                '#description' => $this->t(''),
                '#required' => false,
                      '#options' => array(yes=>'Yes*',no=>'No'),
                '#default_value' => $config->get('field_disability_by_birth'),
            ];
            $form['field_disability_since'] = [
                '#type' => 'number',
                '#title' => $this->t('(8) Disability Since :<span style="color:red">*</span>'),
                '#description' => $this->t(''),
                '#required' => false,
                '#default_value' => $config->get('  field_disability_certificate'),
            ];
            $form['field_pension_card_number'] = [
                '#type' => 'textfield',
                '#title' => $this->t('(9) Pension Card Number :<span style="color:red">*</span>'),
                '#description' => $this->t(''),
                '#required' => false,
                '#default_value' => $config->get('field_pension_card_number'),
            ];
            $form['field_disability_scheme'] = [
                '#type' => 'textfield',
                '#title' => $this->t('(10) Disability Scheme :<span style="color:red">*</span>'),
                '#description' => $this->t(''),
                '#required' => false,
                '#default_value' => $config->get('field_disability_scheme'),
            ];
            $form['field_pension_card_number'] = [
                '#type' => 'textfield',
                '#title' => $this->t('(11) Pension Card Number :<span style="color:red">*</span>'),
                '#description' => $this->t(''),
                '#required' => false,
                '#default_value' => $config->get('field_pension_card_number'),
            ];
            $form['field_hospital_treatingdisabilit'] = [
                '#type' => 'textfield',
                '#title' => $this->t('(11) Hospital Treating Disability :<span style="color:red">*</span>'),
                '#description' => $this->t(''),
                '#required' => false,
                '#default_value' => $config->get('field_hospital_treatingdisabilit'),
            ];
            $form['field_disability_area'] = [
                '#type' => 'checkboxes',
                '#title' => $this->t('(13) Disability Area<span style="color:red">*</span>'),
                '#description' => $this->t(''),
                '#required' => false,
                '#options' =>     $disabilityAreaArray,
                '#default_value' => $config->get('field_disability_area'),
            ];
            $form['field_disability_due_to'] = [
                '#type' => 'radios',
                '#title' => $this->t('(14) Disability Due To :<span style="color:red">*</span>'),
                '#description' => $this->t(''),
                '#required' => false,
                '#options' => array(accident=>'Accident',congenital=>'Congenital',hereditary=>'Hereditary'),
                '#default_value' => $config->get('field_disability_due_to'),
            ];
            $form['field_disability_due_to']['#suffix'] = '</fieldset>';
        // }
        // if ($this->step == 4) {
            $form['field_employed']['#prefix'] = '<fieldset>';
            $form['field_employed'] = [
                '#type' => 'radios',
                '#title' => $this->t('(1) Employed :<span style="color:red">*</span>'),
                '#description' => $this->t(''),
                '#required' => false,
                '#options' => array(yes=>'Yes',no=>'No*'),
                '#default_value' => $config->get('field_employed'),
            ];
            $form['field_occupation'] = [
                '#type' => 'checkboxes',
                '#title' => $this->t('(2) Occupation :<span style="color:red">*</span>'),
                '#description' => $this->t(''),
                '#required' => false,
                '#options' => $occupationArray,
                '#default_value' => $config->get('field_occupation'),
            ];
            $form['field_bpl_apl'] = [
                '#type' => 'radios',
                '#title' => $this->t('(3) BPL/APL :<span style="color:red">*</span>'),
                '#description' => $this->t(''),
                '#required' => false,
                      '#options' => array(N/A=>'N/A',BPL=>'BPL',APL=>'APL',Antodya=>'Antodya'),
                '#default_value' => $config->get('field_bpl_apl'),
            ];
            $form['field_personal_income_annual_'] = [
                '#type' => 'radios',
                '#title' => $this->t('(4) Personal Income(Annual) : :<span style="color:red">*</span>'),
                '#description' => $this->t(''),
                '#required' => false,
                '#options' => $personalIncomeArray,
                '#default_value' => $config->get('field_personal_income_annual_'),
            ];
            $form['field_father_income'] = [
                '#type' => 'radios',
                '#title' => $this->t('(5) Father Income(Annual) :<span style="color:red">*</span>'),
                '#description' => $this->t(''),
                '#required' => false,
                '#options' => $fatherIncomeArray,
                '#default_value' => $config->get('field_father_income'),
            ];
            $form['field_spouse_income'] = [
                '#type' => 'radios',
                '#title' => $this->t('(6) Spouse Income(Annual) :<span style="color:red">*</span>'),
                '#description' => $this->t(''),
                '#required' => false,
                '#options' => $spouseIncomeArray,
                '#default_value' => $config->get('field_spouse_income_annual_'),
            ];
            $form['field_spouse_income_annual_']['#suffix'] = '</fieldset>';
        // }
        // if ($this->step == 5) {
            $form['field_attached_identity_proof']['#prefix'] = '<fieldset>';
            $form['field_attached_identity_proof'] = [
                '#type' => 'checkboxes',
                '#title' => $this->t('(1) Attached Identity Proof :<span style="color:red">*</span>'),
                '#description' => $this->t(''),
                '#required' => false,
                '#options' => array(drivingLicense=>'Driving License',rationCard=>'Ration Card',voterID=>'Voter ID',PANcard=>'PAN card',aadharCard=>'Aadhar Card'),
                '#default_value' => $config->get('field_attached_identity_proof'),
            ];
            $form['field_identity_proof_number'] = [
                '#type' => 'textfield',
                '#title' => $this->t('(2) Identity Proof Number : :<span style="color:red">*</span>'),
                '#description' => $this->t(''),
                '#required' => false,
                '#default_value' => $config->get('field_identity_proof_number'),
            ];
            $form['field_aadhar_card_number'] = [
                '#type' => 'textfield',
                '#title' => $this->t('(3) Aadhar Card Number :<span style="color:red">*</span>'),
                '#description' => $this->t(''),
                '#required' => false,
                '#default_value' => $config->get('field_aadhar_card_number'),
            ];
            $form['field_tin_npr_'] = [
                '#type' => 'textfield',
                '#title' => $this->t('(4) TIN(NPR) :<span style="color:red">*</span>'),
                '#description' => $this->t(''),
                '#required' => false,
                '#default_value' => $config->get('field_tin_npr_'),
            ];
            $form['field_any_other_state_ut_s_id'] = [
                '#type' => 'textfield',
                '#title' => $this->t('(5) Any Other State/UT\'s ID :<span style="color:red">*</span>'),
                '#description' => $this->t(''),
                '#required' => false,
                '#default_value' => $config->get('field_any_other_state_ut_s_id'),
            ];
            $form['field_other_state_ut_s_id_value'] = [
                '#type' => 'textfield',
                '#title' => $this->t('(6) Other State/UT\'s ID value :<span style="color:red">*</span>'),
                '#description' => $this->t(''),
                '#required' => false,
                '#default_value' => $config->get('field_other_state_ut_s_id_value'),
            ];
            $form['field_i_agree'] = [
                '#type' => 'checkboxes',
                '#title' => $this->t('Swearing In :<span style="color:red">*</span>'),
                '#description' => $this->t(''),
                '#required' => false,
                '#options' => array(IAgree=>'I,the applicant do hereby declare that what is stated above is true to the best of my information and brief field everywhere it is used'),//),
                '#default_value' => $config->get('field_i_agree'),
            ];
            $form['field_i_the_applicant_do_hereby_']['#suffix'] = '</fieldset>';
             return $form;
        }
       

  /**
   * {@inheritdoc}
   */
    public function validateForm(array &$form, FormStateInterface $form_state) {

        return parent::validateForm($form, $form_state);
    }

  /**
   * {@inheritdoc}
   */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        $addresProofArray = $form_state->getValue('field_document_for_address_proof');
            $addresProofString = '';
            foreach($addresProofArray as $addresProof) {
            if (!empty($addresProof)) {
                $addresProofString .= $addresProof . ",";
              }
            }
            $addresProofString = rtrim($addresProofString, ',');
             $disabilityTypeArray = $form_state->getValue('field_disability_type');
                $disabilityTypeString = '';
                foreach($disabilityTypeArray as $disabilityType) {
                if (!empty($disabilityType)) {
                    $disabilityTypeString .= $disabilityType . ",";
                  }
                }
                $disabilityTypeString = rtrim($disabilityTypeString, ',');
                $disabilityAreaArray = $form_state->getValue('field_disability_area');
                $disabilityAreaString = '';
                foreach($disabilityAreaArray as $disabilityArea) {
                if (!empty($disabilityArea)) {
                    $disabilityAreaString .= $disabilityArea . ",";
                  }
                }
                $disabilityAreaString = rtrim($disabilityAreaString, ',');
                 $identityProofArray = $form_state->getValue('field_attached_identity_proof');
                $identityProofString = '';
                foreach($identityProofArray as $identityProof) {
                if (!empty($identityProof)) {
                    $identityProofString .= $identityProof . ",";
                  }
                }
                $identityProofString = rtrim($identityProofString, ',');
           
            parent::submitForm($form, $form_state);
            // $node = \Drupal::entityQuery('node')
            //         ->condition('status', 1)
            //         ->execute();
        
            
            // var_dump($node);exit;
                
                $node = entity_create('node', array(
                    'type' => 'personal_details',
                    'title' => 'personal_details',
                    'body' => array(
                        'value' => $form_state->getValue('body'),
                        'format' => 'basic_html',
                    ),
                    'field_applicant_name' => $form_state->getValue('field_applicant_name'),
                    'field_father_s_name' => $form_state->getValue('field_father_s_name'),
                    'field_mother_s_name' => $form_state->getValue('field_mother_s_name'),
                    'field_date_of_birth' => $form_state->getValue('field_date_of_birth'),
                    'field_age' => $form_state->getValue('field_age'),
                    'field_mobile_number' => $form_state->getValue('field_mobile_number'),
                    'field_e_mail_id' => $form_state->getValue('field_e_mail_id'),
                    'field_gender' => $form_state->getValue('field_gender'),
                    'field_mark_of_identification' => $form_state->getValue('field_mark_of_identification'),
                    'field_category' => $form_state->getValue('field_category'),
                    'field_caste_certificate' => $form_state->getValue('field_caste_certificate'),
                   
                    'field_blood_group' => $form_state->getValue('field_blood_group'),
                    'field_marital_status' => $form_state->getValue('field_marital_status'),
                    'field__if_you_are_married_give_s' => $form_state->getValue('field__if_you_are_married_give_s'),
                    'field_name_of_guardian_caretaker' => $form_state->getValue('field_name_of_guardian_caretaker'),
                    'field_guardian_caretaker_contact' => $form_state->getValue('field_guardian_caretaker_contact'),
                    'field_relation_with_person_with_' => $form_state->getValue('field_relation_with_person_with_'),
                    'field_educational_details' => $form_state->getValue('field_educational_details'),
                    'field_photograph' => $form_state->getValue('field_photograph'),
                     'field_correspondence_address_' => $form_state->getValue('field_correspondence_address_'),
                    'field_pincode_of_correspondence_' => $form_state->getValue('field_pincode_of_correspondence_'),
                    'field_pincode_of_correspondence_' => $form_state->getValue('field_pincode_of_correspondence_'),
                    'field_state_ut_s_of_corresponden' => $form_state->getValue('field_state_ut_s_of_corresponden'),
                    'field_district_of_correspondence' => $form_state->getValue('field_district_of_correspondence'),
                    'field_city_district_of_correspon' => $form_state->getValue('field_city_district_of_correspon'),
                  
                   'field_village_block_of_correspon' => $form_state->getValue('field_village_block_of_correspon'),
            
                     'field_document_for_address_proof' => $addresProofString,
                    'field_applicant_permanent_addres' => $form_state->getValue('field_applicant_permanent_addres'),
                    'field_pincode_of_permanent_addre' => $form_state->getValue('field_pincode_of_permanent_addre'),
                    'field_state_ut_s_of_permanent_ad' => $form_state->getValue('field_state_ut_s_of_permanent_ad'),
                    'field_district_of_permanent_addr' => $form_state->getValue('field_district_of_permanent_addr'),
                    'field_city_district_of_permanent' => $form_state->getValue('field_city_district_of_permanent'),
                    'field_village_of_permanent_addre' => $form_state->getValue('field_village_of_permanent_addre'),
                     'field_disability_certificate' => $form_state->getValue('field_disability_certificate'),
                'field_sr_reg_no_of_certificate' => $form_state->getValue('field_sr_reg_no_of_certificate'),
                'field_date_of_issue_' => $form_state->getValue('field_date_of_issue_'),
                'field_disability_percentage_' => $form_state->getValue('field_disability_percentage_'),
                'field_details_of_issuing_authori' => $form_state->getValue('field_details_of_issuing_authori'),
                'field_disability_type' => $form_state->getValue('field_disability_type'),
                'field_disability_by_birth' => $form_state->getValue('field_disability_by_birth'),
                'field_disability_since' => $form_state->getValue('field_disability_since'),
                'field_pension_card_number' => $form_state->getValue('field_pension_card_number'),
                'field_disability_scheme' => $form_state->getValue('field_disability_scheme'),
                'field_hospital_treatingdisabilit' => $form_state->getValue('field_hospital_treatingdisabilit'),
                'field_disability_area' => $form_state->getValue('field_disability_area'),
                'field_disability_due_to' => $form_state->getValue('field_disability_due_to'),
                'field_employed' => $form_state->getValue('field_employed'),
                'field_occupation' => $form_state->getValue('field_occupation'),
                'field_bpl_apl' => $form_state->getValue('field_bpl_apl'),
                'field_personal_income_annual_' => $form_state->getValue('field_personal_income_annual_'),
                'field_father_income' => $form_state->getValue('field_father_income'),
                'field_spouse_income' => $form_state->getValue('field_spouse_income'),
                 'field_attached_identity_proof' => $IdentityProofString,
                'field_identity_proof_number' => $form_state->getValue('field_identity_proof_number'),
                'field_aadhar_card_number' => $form_state->getValue('field_aadhar_card_number'),
                'field_tin_npr_' => $form_state->getValue('field_tin_npr_'),
                'field_any_other_state_ut_s_id' => $form_state->getValue('field_any_other_state_ut_s_id'),
                'field_other_state_ut_s_id_value' => $form_state->getValue('field_other_state_ut_s_id_value'),
                'field_i_agree' => $form_state->getValue('field_i_agree'),
                    //'uid' => $uid,              
                ));
                $node->save();
                drupal_set_message($this->t('<span style="color:green">Personal details are saved</span>'));
      
    }
  }
//}

